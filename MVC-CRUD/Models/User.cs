﻿using System;
using System.Collections.Generic;

namespace MVC_CRUD.Models
{
    public partial class User
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime? Created { get; set; }
    }
}
